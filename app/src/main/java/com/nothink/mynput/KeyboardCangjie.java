package com.nothink.mynput;

import android.content.Context;

public class KeyboardCangjie extends MynputKeyboard {
    private Key[] numKeys = new Key[10];
    protected CangjieTable cjTable;

    public StringBuilder sb;
    public boolean isNumCandidate = false;

    public KeyboardCangjie(Context context) {
        super(context, R.xml.cangjie);
        canShift = true;
        for (Key key: getKeys()) {
            if (key.codes[0] >= '0' && key.codes[0] <= '9') numKeys[key.codes[0] - 48] = key;
        }
        cjTable = new CangjieTable(context);
    }

    public void candidateNum(boolean isCandidate) {
        if (isCandidate) {
            if (sb.length() > 5) return;
            int i = 1;
            for (String word : cjTable.match(sb.toString())) {
                if (i > 9) break;
                numKeys[i].label = word;
                numKeys[i].text = word;
                numKeys[i].popupCharacters = String.valueOf(i);
                i++;
            }
            for (; i < 10; i++) {
                numKeys[i].label = " ";
                numKeys[i].text = "";
            }
            int totalCandidate = cjTable.matches.size();
            if (totalCandidate <= 10) {
                numKeys[0].label = " ";
                numKeys[0].text = "";
                if (totalCandidate == 10) {
                    String word = cjTable.matches.get(9);
                    numKeys[0].label = word;
                    numKeys[0].text = word;
                    numKeys[0].popupCharacters = "10";
                }
            } else {
                numKeys[0].codes[0] = MynputKeyboardView.KEYCODE_CANDIDATE_SWITCH;
                numKeys[0].label = "\uFE19";
                numKeys[0].text = null;
                numKeys[0].popupCharacters = null;
            }
        } else {
            numKeys[0].codes[0] = 48;
            for (MynputKeyboard.Key key: numKeys) {
                key.label = String.valueOf((char)key.codes[0]);
                key.text = null;
                key.popupCharacters = null;
            }
        }
        this.isNumCandidate = isCandidate;
    }

    @Override
    public int handleAlpha(int primaryCode) {
        if (isShifted()) {
            ims.getCurrentInputConnection().finishComposingText();
            cleanComposingText();
            return super.handleAlpha(primaryCode);
        }
        sb.append((char) primaryCode);
        ims.getCurrentInputConnection().setComposingText(sb, 1);
        candidateNum(true);
        return 0;
    }

    @Override
    public int handleBackspace() {
        final int length = sb.length();
        if (length == 0) return super.handleBackspace();
        if (length == 1) cleanComposingText();
        else if (length > 1) {
            ims.getCurrentInputConnection().setComposingText(sb.deleteCharAt(length - 1), 1);
            candidateNum(true);
        }
        return 0;
    }

    @Override
    public int handleEnter() {
        cleanComposingText();
        return super.handleEnter();
    }

    @Override
    public int handleSpecial(int primaryCode) {
        if (isNumCandidate) {
            if (primaryCode == '0') return 0;
            else if (primaryCode >= '1' && primaryCode <= '9') {
                CharSequence text = numKeys[Integer.parseInt(String.valueOf((char) primaryCode))].text;
                if (text != "") {
                    cleanComposingText();
                    ims.getCurrentInputConnection().commitText(text, 1);
                }
                return 0;
            }
            ims.getCurrentInputConnection().finishComposingText();
            cleanComposingText();
        }
        return primaryCode;
    }

    public void cleanComposingText() {
        sb.setLength(0);
        ims.getCurrentInputConnection().setComposingText("", 1);
        candidateNum(false);
    }

    public Key getNumKey(int num) {
        return numKeys[num];
    }
}