package com.nothink.mynput;

import android.content.res.Configuration;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;

public class MynputME extends InputMethodService implements KeyboardView.OnKeyboardActionListener {
    private InputMethodManager imm;
    private MynputKeyboardView kbView;

    private EditorInfo eInfo;
    protected boolean hasHardKeyboard;

    @Override
    public void onCreate() {
        super.onCreate();
        imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        Configuration conf = getResources().getConfiguration();
        hasHardKeyboard = (conf.keyboard != Configuration.KEYBOARD_NOKEYS);
    }

    @Override
    public void onInitializeInterface() {
        kbView = (MynputKeyboardView) getLayoutInflater().inflate(R.layout.keyboard, null);

        kbView.cangjieKB.sb = new StringBuilder();
    }

    @Override
    public View onCreateInputView() {
        kbView.setPreviewEnabled(false);
        kbView.setOnKeyboardActionListener(this);
        return kbView;
    }

    @Override
    public void onStartInput(EditorInfo attribute, boolean restarting) {
        super.onStartInput(attribute, restarting);
        eInfo = attribute;

        kbView.initialKeyboard(eInfo);
    }

    @Override
    public void onFinishInput() {
        super.onFinishInput();
        getCurrentInputConnection().finishComposingText();
        kbView.cangjieKB.cleanComposingText();
    }

    @Override
    public void onFinishInputView(boolean finishingInput) {
        getCurrentInputConnection().finishComposingText();
        super.onFinishInputView(finishingInput);
        kbView.cangjieKB.cleanComposingText();
    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {
        if (kbView.onChangeKeyboardKey(primaryCode)) return;

        int resultCode = primaryCode;
        switch (primaryCode) {
            case Keyboard.KEYCODE_DELETE:
                resultCode = kbView.curKB.handleBackspace();
                break;
            case Keyboard.KEYCODE_DONE:
                resultCode = kbView.curKB.handleEnter();
                break;
            case MynputKeyboardView.KEYCODE_LANGUAGE_SWITCH:
                imm.showInputMethodPicker();
                break;
            default:
                resultCode = primaryCode >= 'a' && primaryCode <= 'z' ? kbView.curKB.handleAlpha(primaryCode) : kbView.curKB.handleSpecial(primaryCode);
                break;
        }
        type(resultCode);
        kbView.invalidateAllKeys();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (hasHardKeyboard && kbView.isShown()) {
            if ((event.isShiftPressed() && keyCode == KeyEvent.KEYCODE_SPACE) || keyCode == 1000) {
                onKey(MynputKeyboardView.KEYCODE_LANGUAGE_SWITCH, null);
                return true;
            }

            if (kbView.isTableKB()) {
                if (kbView.cangjieKB.sb.length() > 0) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DEL:
                            onKey(Keyboard.KEYCODE_DELETE, null);
                            return true;
                        case KeyEvent.KEYCODE_ENTER:
                            getCurrentInputConnection().finishComposingText();
                            kbView.cangjieKB.cleanComposingText();
                            kbView.invalidateAllKeys();
                            return true;
                        case KeyEvent.KEYCODE_SPACE:
                            onKey('1', null);
                            return true;
                    }
                }

                if ((keyCode >= KeyEvent.KEYCODE_1 && keyCode <= KeyEvent.KEYCODE_9) || (keyCode >= KeyEvent.KEYCODE_A && keyCode <= KeyEvent.KEYCODE_Z)) {
                    onKey(event.getUnicodeChar(), null);
                    return true;
                }
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onPress(int primaryCode) {

    }

    @Override
    public void onRelease(int primaryCode) {

    }

    @Override
    public void onText(CharSequence text) {
        if (text != "") {
            kbView.cangjieKB.cleanComposingText();
            getCurrentInputConnection().commitText(text, 1);
            kbView.invalidateAllKeys();
        }
    }

    @Override
    public void swipeLeft() {

    }

    @Override
    public void swipeRight() {

    }

    @Override
    public void swipeDown() {

    }

    @Override
    public void swipeUp() {

    }

    // customize method
    private void type(int primaryCode) {
        if (primaryCode > 0) getCurrentInputConnection().commitText(String.valueOf((char) primaryCode), 1);
    }
}
