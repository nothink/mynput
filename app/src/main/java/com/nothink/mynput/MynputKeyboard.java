package com.nothink.mynput;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.text.InputType;
import android.view.KeyEvent;

public class MynputKeyboard extends Keyboard {
    protected InputMethodService ims;

    protected boolean canShift = false;

    public MynputKeyboard(Context context, int xmlLayoutResId) {
        super(context, xmlLayoutResId);
        this.ims = (InputMethodService) context;
    }

    @Override
    protected Key createKeyFromXml(Resources res, Row parent, int x, int y, XmlResourceParser parser) {
        return new MynputKey(res, parent, x, y, parser);
    }

    static class MynputKey extends Key {
        public MynputKey(Resources res, Row parent, int x, int y, XmlResourceParser parser) {
            super(res, parent, x, y, parser);
        }
    }

    public int handleAlpha(int primaryCode) {
        if (isShifted()) primaryCode = Character.toUpperCase(primaryCode);
        return primaryCode;
    }

    public int handleBackspace() {
        keyDownUp(KeyEvent.KEYCODE_DEL);
        return 0;
    }

    public int handleEnter() {
        int flags = (ims.getCurrentInputEditorInfo().inputType & InputType.TYPE_MASK_FLAGS);
        if (flags == InputType.TYPE_TEXT_FLAG_MULTI_LINE || flags == InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE) return 10;
        else keyDownUp(KeyEvent.KEYCODE_ENTER);
        return 0;
    }

    public int handleSpecial(int primaryCode) {
        return primaryCode;
    }

    protected void keyDownUp(int keyEventCode) {
        ims.getCurrentInputConnection().sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, keyEventCode));
        ims.getCurrentInputConnection().sendKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, keyEventCode));
    }
}
