package com.nothink.mynput;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;

public class CangjieTable {
    private JSONObject table;
    ArrayList<String>[] matchesArray;
    ArrayList<String> matches;

    public CangjieTable(Context context) {
        try {
            InputStream is = context.getAssets().open("cj5.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            table = new JSONObject(new String(buffer, StandardCharsets.UTF_8));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public ArrayList<String> match(String code) {
        JSONObject json = table;
        int cc = code.length();
        matches = new ArrayList<>();
        matchesArray = new ArrayList[5];
        for (int i = 0; i < matchesArray.length; i++) {
            matchesArray[i] = new ArrayList<>();
        }

        try {
            for (String c : code.split("(?!^)")) {
                if (json.has(c)) {
                    cc--;
                    json = json.getJSONObject(c);
                } else break;
            }
            setCandidate(json, cc);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (ArrayList<String> al: matchesArray) {
            matches.addAll(al);
        }
        return matches;
    }

    private void addWordsByCodes(JSONObject json, int arrIndex) {
        try {
            if (json.has("0")) {
                JSONArray words = json.getJSONArray("0");
                for (int i = 0; i < words.length(); i++) {
                    matchesArray[arrIndex].add(words.getString(i));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setCandidate(JSONObject json, int index) {
        try {
            for (Iterator<String> it = json.keys(); it.hasNext(); ) {
                String key = it.next();

                if (key.equals("0")) addWordsByCodes(json, index);
                else setCandidate(json.getJSONObject(key), index + 1);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
