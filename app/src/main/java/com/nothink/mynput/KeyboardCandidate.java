package com.nothink.mynput;

import android.content.Context;

import java.util.ArrayList;

public class KeyboardCandidate extends MynputKeyboard {
    private ArrayList<Key> candidateKeys = new ArrayList<>();
    private KeyboardCangjie cjKB;

    public int curPage, maxPage;

    public KeyboardCandidate(Context context, KeyboardCangjie cangjiejKeyBoard) {
        super(context, R.xml.candidate);
        for (Key key: getKeys()) {
            if (key.label.equals(" ")) candidateKeys.add(key);
        }
        this.cjKB = cangjiejKeyBoard;
    }

    public void candidateView() {
        int next = (curPage - 1) * candidateKeys.size() + 9;
        int to = Math.min(candidateKeys.size(), cjKB.cjTable.matches.size() - next);
        int i = 0;
        for (; i < to; i++) {
            candidateKeys.get(i).label = cjKB.cjTable.matches.get(next);
            candidateKeys.get(i).text = cjKB.cjTable.matches.get(next);
            next++;
        }
        for (; i < candidateKeys.size(); i++) {
            candidateKeys.get(i).label = " ";
            candidateKeys.get(i).text = "";
        }
    }

    public void initial() {
        curPage = 1;
        maxPage = candidateKeys.size() != 0 ? (int)Math.ceil((cjKB.cjTable.matches.size() - 9) / candidateKeys.size()) : 1;
    }
}