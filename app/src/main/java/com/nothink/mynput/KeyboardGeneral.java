package com.nothink.mynput;

import android.content.Context;

public class KeyboardGeneral extends MynputKeyboard {

    public KeyboardGeneral(Context context) {
        super(context, R.xml.general);
        canShift = true;
    }

    @Override
    public int handleAlpha(int primaryCode) {
        primaryCode = super.handleAlpha(primaryCode);
        setShifted(false);
        return primaryCode;
    }
}