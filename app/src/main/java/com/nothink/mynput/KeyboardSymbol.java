package com.nothink.mynput;

import android.content.Context;

public class KeyboardSymbol extends MynputKeyboard {

    public KeyboardSymbol(Context context) {
        super(context, R.xml.symbol);
    }
}