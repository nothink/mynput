package com.nothink.mynput;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.Keyboard.Key;
import android.inputmethodservice.KeyboardView;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.inputmethod.EditorInfo;

public class MynputKeyboardView extends KeyboardView {
    static final int KEYCODE_LANGUAGE_SWITCH = -100;
    static final int KEYCODE_SYMBOL_SWITCH = -200;
    static final int KEYCODE_CANDIDATE_SWITCH = -300;
    static final int KEYCODE_CANDIDATE_PREV = -310;
    static final int KEYCODE_CANDIDATE_NEXT = -320;

    public KeyboardCangjie cangjieKB;
    public KeyboardCandidate candidateKB;
    public KeyboardGeneral generalKB;
    public KeyboardSymbol symbolKB;
    public MynputKeyboard curKB;

    private InputMethodService ims;
    private Paint paint = new Paint();

    public MynputKeyboardView(Context context, AttributeSet attributes) {
        super(context, attributes);
        this.ims = (InputMethodService) context;

        cangjieKB = new KeyboardCangjie(context);
        candidateKB = new KeyboardCandidate(context, cangjieKB);
        generalKB = new KeyboardGeneral(context);
        symbolKB = new KeyboardSymbol(context);
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(40);
        paint.setColor(Color.BLACK);
        for (Key key : getKeyboard().getKeys()) {
            if (key.codes[0] == -5) {
                canvas.drawText("DEL", key.x + key.width / 2, key.y + 50, paint);
            } else if (key.popupCharacters != null) {
                if (key.codes[0] >= 'a' && key.codes[0] <= 'z') {
                    String word = key.popupCharacters.toString();
                    if (this.isShifted()) word = word.toUpperCase();
                    canvas.drawText(word, key.x + (key.width - 25), key.y + 50, paint);
                }
                else canvas.drawText(key.popupCharacters.toString(), key.x + 25, key.y + 50, paint);
            }
        }
    }



    @Override
    protected boolean onLongPress(Key key) {
        if (key.codes[0] >= '0' && key.codes[0] <= '9') {
            getOnKeyboardActionListener().onKey(key.codes[0], null);
            return true;
        } else if (key.codes[0] == Keyboard.KEYCODE_MODE_CHANGE) {
            getOnKeyboardActionListener().onKey(KEYCODE_LANGUAGE_SWITCH, null);
            return true;
        } else {
            return super.onLongPress(key);
        }
    }

    public void initialKeyboard(EditorInfo editorInfo) {
        switch (editorInfo.inputType & InputType.TYPE_MASK_CLASS) {
            case InputType.TYPE_CLASS_NUMBER:
            case InputType.TYPE_CLASS_PHONE:
            case InputType.TYPE_CLASS_DATETIME:
                setCurKB(symbolKB);
                break;

            case InputType.TYPE_CLASS_TEXT:
                switch (editorInfo.inputType & InputType.TYPE_MASK_VARIATION) {
                    case InputType.TYPE_NUMBER_VARIATION_PASSWORD:
                    case InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS:
                    case InputType.TYPE_TEXT_VARIATION_PASSWORD:
                    case InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD:
                    case InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD:
                        setCurKB(generalKB);
                        break;
                    default:
                        setCurKB(cangjieKB);
                        break;
                }
                break;
            default:
                setCurKB(cangjieKB);
                break;
        }

        if (curKB == generalKB) {
            int caps = editorInfo.inputType != InputType.TYPE_NULL ? ims.getCurrentInputConnection().getCursorCapsMode(editorInfo.inputType) : 0;
            setShifted(caps != 0);
        } else if (curKB == cangjieKB) {
            cangjieKB.cleanComposingText();
        }
    }

    public boolean isTableKB() {
        return curKB == cangjieKB;
    }
    
    public boolean onChangeKeyboardKey(int primaryCode) {
        switch (primaryCode) {
            case Keyboard.KEYCODE_MODE_CHANGE:
                if (cangjieKB.sb.length() > 0) return false;
                setCurKB(curKB == cangjieKB ? generalKB : cangjieKB);
                break;
            case Keyboard.KEYCODE_SHIFT:
                if (curKB.canShift) setShifted(!curKB.isShifted());
                break;
            case KEYCODE_SYMBOL_SWITCH:
                setKeyboard(getKeyboard() == symbolKB ? curKB : symbolKB);
                break;
            case KEYCODE_CANDIDATE_SWITCH:
                if (curKB == cangjieKB) {
                    candidateKB.initial();
                    candidateKB.candidateView();
                    setCurKB(candidateKB);
                }
                else setCurKB(cangjieKB);
                break;
            case MynputKeyboardView.KEYCODE_CANDIDATE_PREV:
                if (candidateKB.curPage > 1) {
                    candidateKB.curPage--;
                    candidateKB.candidateView();
                    invalidateAllKeys();
                }
                break;
            case MynputKeyboardView.KEYCODE_CANDIDATE_NEXT:
                if (candidateKB.curPage < candidateKB.maxPage) {
                    candidateKB.curPage++;
                    candidateKB.candidateView();
                    invalidateAllKeys();
                }
                break;
            default:
                return false;
        }
        return true;
    }
    
    public void setCurKB(MynputKeyboard keyboard) {
        curKB = keyboard;
        setKeyboard(curKB);
    }
}